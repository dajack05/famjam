const GenerateItemHTML = game => `  <a class="item" href="${game.link}">
                                        <div class="icon">
                                            <img src="${game.image}" />
                                        </div>
                                        <div>
                                            <h2>${game.title}</h2>
                                            <table class="credits">
                                                <tr>
                                                  <td>
                                                    <b>Lead Writer</b>:</td><td> ${game.credits.writer}
                                                  </td>
                                                  <td>
                                                    <b>Lead Technical Artist</b>:</td><td> ${game.credits.technicalArtist}
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <b>UI/UX Designer</b>:</td><td> ${game.credits.UIUXDesigner}
                                                  </td>
                                                  <td>
                                                    <b>Original Score</b>:</td><td> ${game.credits.score}
                                                  </td>
                                                </tr>
                                            </table>
                                            <hr/>
                                            <p>${game.description}</p>
                                        </div>
                                    </a>`;

if (GAMES.length > 0) {
    const content = document.getElementById('content');
    if (content) {
        for (const game of GAMES) {
            content.innerHTML += GenerateItemHTML(game);
        }
    }
}

document.querySelectorAll(".item").forEach(elem => elem.addEventListener('click', onItemClick.bind(this)));

function onItemClick(evt) {
    console.log(evt.target.attributes);
    const attrs = evt.target.attributes;
    if (attrs.getNamedItem("href")) {
        //Has href
        window.location = attrs.getNamedItem("href").value;
    }
}