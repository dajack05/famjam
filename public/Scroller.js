const toChange = document.getElementById('header');
const buttonDiv = document.getElementById('controls');

document.addEventListener('scroll', (evt) => {
    const scroll = -document.body.getClientRects()[0].top;
    if (scroll > 0) hideHeader();
    else showHeader();
});

document.querySelectorAll("button").forEach(elem => elem.addEventListener('click', onBtnClick.bind(this)));

function onBtnClick(evt) {
    const attrs = evt.target.attributes;
    if (attrs.getNamedItem("href")) {
        //Has href
        window.location = attrs.getNamedItem("href").value;
    }
}

function showHeader() {
    toChange.classList.remove('header-scrolling');
    buttonDiv.classList.remove('hidden');
}

function hideHeader() {
    toChange.classList.add('header-scrolling');
    buttonDiv.classList.add('hidden');
}