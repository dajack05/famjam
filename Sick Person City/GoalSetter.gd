extends Node

var current_goal = Vector3(0,0,0)
var is_pickup = true

var time_remaining = 60
var score = 0

onready var timeRemainingLabel = $CanvasLayer/HBoxContainer/TimeRemainingLabel as Label
onready var scoreLabel = $CanvasLayer/HBoxContainer2/ScoreLabel as Label
onready var finalScoreLabel = $CanvasLayer/GameoverHolder/VBoxContainer/FinalScoreLabel as Label

func _ready():
	randomize()
	pickNewPatient()
	evalGates()
	
func evalGates():
	#Set Goals visibility
	$Pickup.visible = is_pickup
	$Dropoff.visible = !is_pickup
	$PeopleHolder/People.visible = is_pickup

	#Set Goals location
	$Pickup.transform.origin = current_goal
	$Dropoff.transform.origin = current_goal
	$PeopleHolder.transform.origin = current_goal
	
func pickNewPatient():
	var idx = round(rand_range(0,$PickupSpawns.get_children().size()-1))
	current_goal = $PickupSpawns.get_child(idx).transform.origin
	$PeopleHolder/People.choose()

func _process(delta):
	time_remaining -= delta
	
	var timeString = String(round(time_remaining))
	
	timeRemainingLabel.text = timeString
	scoreLabel.text = String(score)
	
	if time_remaining <= 0:
		#Gameover!
		$CanvasLayer/GameoverHolder.visible = true
		finalScoreLabel.text = "Final Score: " + String(score)
	
	time_remaining = clamp(time_remaining,0,1000)
	
	if $Area.transform.origin != current_goal:
		$Area.transform.origin = current_goal;

var time_to_add = 15
func _on_Area_body_entered(body:PhysicsBody):
	if body.is_in_group("player"):
		#Time to set next goal!
		is_pickup = !is_pickup
		
		if is_pickup:
			#Just finished a dropoff
			#Add to score and add 15s
			score += 1
			time_remaining += time_to_add
			time_to_add -= 2
			if time_to_add < 5:
				time_to_add = 5
			
			#Find and set a new one
			pickNewPatient()
			
			#Play "thank you" audio
			$ThankYouAudio.play()
		else:
			#Head back to the hospital!
			current_goal = $DropoffSpawn.transform.origin
	
		evalGates()
