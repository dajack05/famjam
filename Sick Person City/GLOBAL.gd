extends Node

func play():
	if !$ThemeAudio.playing:
		$ThemeAudio.play()

func stop():
	$ThemeAudio.stop()
