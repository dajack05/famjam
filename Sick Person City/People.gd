extends Spatial

onready var GoalSetter = get_node("../../")
onready var HelpAudio = get_node("../HelpAudio")
onready var OuchAudio = get_node("../OuchAudio")

func choose():
	var idx = round(rand_range(0,get_child_count()-1))
	for i in range(0,get_child_count()):
		get_child(i).visible = i == idx


func _on_Area_body_entered(body:PhysicsBody):
	if body.is_in_group("player") && GoalSetter.is_pickup:
		if randf() > 0.5:
			HelpAudio.play()
		else:
			OuchAudio.play()
