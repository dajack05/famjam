extends Camera

export(NodePath) var targetNodePath
onready var target = get_node(targetNodePath)

export(NodePath) var lookatNodePath
onready var lookat = get_node(lookatNodePath)

export(NodePath) var AmbulanceNodePath
onready var Ambulance = get_node(AmbulanceNodePath)

export(float) var speed = 1

func _process(delta):
	
	if Input.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://Menu.tscn")
	
	look_at(lookat.get_global_transform().origin,Vector3.UP)
	
	var target = get_node(targetNodePath)
	
	transform.origin = lerp(transform.origin, target.get_global_transform().origin, speed*delta)
	
	var speed = Ambulance.linear_velocity.length() / 7
	var pitch = speed * 3.0 + 1.0
	$EngineSound.pitch_scale = pitch
