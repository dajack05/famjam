extends VehicleBody

export(bool) var enable = true

export(float) var engine_acceleration = 1.0
export(float) var max_engine_speed = 75.0

export(float) var max_steer_angle = 45.0
export(float) var steer_speed = 0.05
export(float) var steer_centering = 2.0

onready var GoalSetter = get_parent().find_node("GoalSetter", true)

var throttle = 0
var target_steering = 0

func _ready():
	$AnimationPlayer.play("default")

func _physics_process(delta):
	if GoalSetter:
		if GoalSetter.time_remaining <= 0:
			#Gameover
			enable = false
			throttle = 0
			target_steering = 0
		
	if enable:
		if Input.is_action_pressed("ui_up"):
			throttle += delta
		elif Input.is_action_pressed("ui_down"):
			throttle -= delta
		
		throttle = clamp(throttle,-1,1)
		
		if Input.is_action_pressed("ui_left"):
			target_steering += 1 * delta;
		if Input.is_action_pressed("ui_right"):
			target_steering -= 1 * delta;
	
		target_steering = clamp(target_steering, -max_steer_angle, max_steer_angle)
	
		# Now make it so (number one)
		if linear_velocity.length() > max_engine_speed:
			throttle = 0
		
		engine_force = throttle * engine_acceleration
			
		steering = lerp(steering, target_steering, steer_speed)
	
		#Damping
		throttle = lerp(throttle, 0, 1 * delta)
		target_steering = lerp(target_steering, 0, steer_centering * delta)
