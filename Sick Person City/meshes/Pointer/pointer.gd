extends Spatial

export(NodePath) var pathToGoalSetter;
onready var GoalSetter = get_node(pathToGoalSetter)

func _process(delta):
	look_at(Vector3(
		GoalSetter.current_goal.x,
		get_global_transform().origin.y,
		GoalSetter.current_goal.z), Vector3.UP)
