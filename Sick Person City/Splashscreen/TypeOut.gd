extends Label

func _ready():
	#Start delay
	yield(get_tree().create_timer(1.0),"timeout")
	
	#Let's start typing!
	$TypeAudio.play()
	
	var charIdx = 0

	while charIdx < text.length():
		while charIdx < text.length() && !text.substr(charIdx).begins_with(" ") && !text.substr(charIdx).begins_with("\n"):
			charIdx += 1
			visible_characters += 1
			yield(get_tree().create_timer(1.0/8),"timeout")
		
		while charIdx < text.length() && text.substr(charIdx).begins_with(" ") || text.substr(charIdx).begins_with("\n"):
			charIdx += 1
			
		#Wait after the word 8 ticks
		yield(get_tree().create_timer(0.5),"timeout")
	
	yield(get_tree().create_timer(1.0),"timeout")
	
	get_tree().change_scene("res://Menu.tscn")
