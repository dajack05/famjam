extends KinematicBody

var gravity = 10
export(float) var movespeed = 10
export(float) var dist = 3

export(NodePath) var target_node_path

onready var target = get_node(target_node_path)

func _process(_delta):
	if target:
		var dist = transform.origin.distance_to(target.transform.origin)
		var x = 0.2 + ((1 / dist) * 10.0)
		$SpookAudio.pitch_scale = x

func _physics_process(_delta):
	if target && transform.origin.distance_to(target.transform.origin) > dist:
		var move_vec = transform.origin.direction_to(target.transform.origin)
		move_vec = move_vec * movespeed
		move_vec.y = -gravity
		move_and_slide(move_vec)
