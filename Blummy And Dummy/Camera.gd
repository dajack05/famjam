extends Camera

export(float) var speed = 1.0

export(Vector3) var offset = Vector3(0,12,20)

export(NodePath) var target_path
onready var target = get_node(target_path)

func _process(delta):
	if target:
		look_at(target.transform.origin,Vector3.UP)
		transform.origin = lerp(transform.origin,target.transform.origin + offset, speed)
