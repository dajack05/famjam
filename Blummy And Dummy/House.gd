extends Spatial

export(String) var player_body_name

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.name == player_body_name:
		#you win!
		get_tree().quit()
