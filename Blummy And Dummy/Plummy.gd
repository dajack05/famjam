extends KinematicBody

var gravity = 10
export(float) var movespeed = 10

export(NodePath) var start_nodes_root_path
onready var start_nodes = get_node(start_nodes_root_path).get_children()

func _ready():
	randomize()
	transform.origin = start_nodes[randi() % start_nodes.size()].transform.origin

func _physics_process(delta):
	var move_vec = Vector3(0,-gravity,0)

	if Input.is_action_pressed("ui_left"):
		move_vec.x += -movespeed
	if Input.is_action_pressed("ui_right"):
		move_vec.x += movespeed
	if Input.is_action_pressed("ui_up"):
		move_vec.z += -movespeed
	if Input.is_action_pressed("ui_down"):
		move_vec.z += movespeed
		
	if abs(move_vec.x) > 0 || abs(move_vec.z) > 0:
		$AnimationPlayer.playback_speed = 2
		set_anim("walk")
	else:
		$AnimationPlayer.playback_speed = 1
		set_anim("idle")

	move_and_slide(move_vec)

func _on_Area_body_entered(body:PhysicsBody):
	if body:
		match body.name:
			"Badguy":
				if randf() > 0.5:
					$DamageAudio.play()
				else:
					$DamageAudio2.play()
			"House":
				$WinAudio.play()


func _on_WinAudio_finished():
	get_tree().change_scene("res://win.tscn")
	
var current_anim = ""
func set_anim(anim:String):
	if current_anim != anim:
		current_anim = anim
		$AnimationPlayer.play(anim)
