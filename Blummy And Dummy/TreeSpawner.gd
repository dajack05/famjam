tool

extends MultiMeshInstance

export(float) var size = 10 setget on_size_change
export(float) var cutoff = 128 setget on_cutoff_change
export(int) var trees_per_pixel = 1 setget on_tpp_change
export(float) var random = 2 setget on_random_change

export(Image) var map_texture = null

export(NodePath) var camera_path
onready var camera:Camera = get_node(camera_path)

const hide_dist = 20
const show_dist = hide_dist * 2
const collision_offset = Vector3(0,-5,8)

var is_ready = false

func on_tpp_change(v):
	trees_per_pixel = v
	if is_ready:
		generate()

func on_cutoff_change(v):
	cutoff = v
	if is_ready:
		generate()

func on_size_change(v):
	size = v
	if is_ready:
		generate()

func on_random_change(v):
	random = v
	if is_ready:
		generate()

func _process(delta):
	if !Engine.editor_hint:
		for i in multimesh.instance_count:
			var pos = multimesh.get_instance_transform(i) as Transform
			var dist = (pos.origin + transform.origin).distance_to(camera.global_transform.origin + collision_offset)
			if dist < hide_dist:
				pos.origin.y = -30
				multimesh.set_instance_transform(i,pos)
			elif pos.origin.y != -30 || dist > show_dist:
				pos.origin.y = 0
				multimesh.set_instance_transform(i,pos)

func generate():
	if Engine.editor_hint:
		if map_texture:

			for child in get_children():
				remove_child(child)
				child.free()
				
			map_texture.lock()
			var data = map_texture.get_data()
			map_texture.unlock()
			
			var height = map_texture.get_height()
			var width = map_texture.get_width()
			
			#Make instance
			multimesh.instance_count = width * height
			
			for y in height:
				for x in width:
					var index = y * width + x
					var value = data[index*4]
					if randf()*cutoff > value:
						for i in trees_per_pixel:
							var offset = randf()
							var origin_x = x * size + (randf() * random)
							var origin_z = y * size + (randf() * random)
							
							var basis = Basis(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,-1,0))
							var trans:Transform = Transform(basis,Vector3(origin_x,0,origin_z))
							#trans = trans.scaled(Vector3(0.6 + offset / 2,0.6 + offset,0.6 + offset / 2))
							
							multimesh.set_instance_transform(y*width+x,trans)
							
							#var tree = instance.instance()
							#tree.transform.origin.x = x * size + (randf() * random)
							#tree.transform.origin.z = y * size + (randf() * random)
							#tree.scale.x = 0.6 + offset / 2
							#tree.scale.y = 0.6 + offset 
							#tree.scale.z = 0.6 + offset / 2
							#tree.rotation.y = randf()
							#add_child(tree)
							#tree.owner = owner

func _on_Spawner_ready():
	is_ready = true
	print_debug(get_children().size())
	if get_children().size() <= 0:
		generate()
