extends Control


func _on_PlayBtn_pressed():
	get_tree().change_scene("res://instructions.tscn")


func _on_StoryBtn_pressed():
	get_tree().change_scene("res://story.tscn")


func _on_QuitBtn_pressed():
	get_tree().quit()
