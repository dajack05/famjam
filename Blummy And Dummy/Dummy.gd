extends KinematicBody

var gravity = 10
export(float) var movespeed = 10
export(float) var dist = 10

export(NodePath) var target_node_path

onready var target = get_node(target_node_path)

func _ready():
	transform.origin = target.transform.origin

func _physics_process(delta):
	if target && transform.origin.distance_to(target.transform.origin) > dist:
		var move_vec = transform.origin.direction_to(target.transform.origin)
		move_vec = move_vec * movespeed
		move_vec.y = -gravity
		move_and_slide(move_vec)
